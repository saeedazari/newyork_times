package ir.saeedazari.nytartichel;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import cz.msebera.android.httpclient.Header;
import ir.saeedazari.nytartichel.adaptors.articleListAdapter;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
ListView article_list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        article_list = (ListView)findViewById(R.id.article_list);
        final String url = "https://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=6018832367d1488d993b2da0aeabf710";
        getDetailByAsync(url);


    }

    public void getDetailByAsync(String url) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(50000);
        client.setMaxRetriesAndTimeout(5, 50000);
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                setDetailFromServerResponse(responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }

    void setDetailFromServerResponse(String serverResponse) {
        try {
            JSONObject allObject = new JSONObject(serverResponse);
            final JSONArray arrryValue = allObject.getJSONObject("response").getJSONArray("docs");
            final String[] arrurl = new String[arrryValue.length()];
            final String[] arrname = new String[arrryValue.length()];

            for (int i = 0; i < arrryValue.length(); i++) {
                arrurl[i] = arrryValue.getJSONObject(i).getString("web_url");
                arrname[i] = arrryValue.getJSONObject(i).getJSONObject("headline").getString("main");
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    articleListAdapter adapter = new articleListAdapter(arrname,arrurl, MainActivity.this);
                    article_list.setAdapter(adapter);
                    article_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            String clicked =
                                    adapterView.getItemAtPosition(position).toString() ;

                            Uri uri = Uri.parse(clicked);
                             Intent intent = new Intent();
                            intent.setData(uri);
                            MainActivity.this.startActivity(intent);
                        }
                    });


                }
            });

        } catch (Exception e) {
            Log.e("Error in Json Parse",e.getMessage());
        }


    }
}
