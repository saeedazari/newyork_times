package ir.saeedazari.nytartichel.adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import ir.saeedazari.nytartichel.R;


/**
 * Created by amirhossein on 4/28/2017.
 */

public class articleListAdapter extends BaseAdapter {
    String main[];
    String web_url[];
    Context mContext;

    public articleListAdapter(String[] mains,String[] web_url, Context mContext) {
        this.main = mains;
        this.mContext = mContext;
        this.web_url =web_url;
    }

    @Override
    public int getCount() {
        return main.length;
    }

    @Override
    public Object getItem(int position) {
        return web_url[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).
                inflate(R.layout.student_list_item, viewGroup, false);
        TextView name = (TextView) rowView.findViewById(R.id.name);
        name.setText(main[position]);
        return rowView;
    }
}
